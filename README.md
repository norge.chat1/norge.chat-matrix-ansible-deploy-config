# Config filer for norge.chat

Norge.chat er basert på det fantastiske prosjektet [Matrix-docker-ansible-deploy](https://github.com/spantaleev/matrix-docker-ansible-deploy).

### Hva gjør de forskjellige filene i dette repoet?

#### hosts
Hosts filen sier hvilken server playbooken skal kjøre mot. I dette tilfellet er det en Hetzner VPS.

Filsti:
```
./inventory/hosts
```

#### vars.yml
Vars filen er konfigurasjonsfilen. Man kan lese mer om hva configen gjør [her](https://github.com/spantaleev/matrix-docker-ansible-deploy/blob/master/docs/configuring-playbook.md).

Filsti:
```
./inventory/host_vars/matrix.norge.chat/vars.yml
```

#### vault
Passord fil. Sensitiv informasjon blir lagt her. Denne filen er kryptert, og ikke lesbar. Den blir referert til av både `hosts` og `vars.yml` filen.

Filsti:
```
./inventory/host_vars/matrix.norge.chat/vault
```

